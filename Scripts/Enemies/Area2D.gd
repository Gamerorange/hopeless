extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal dmg

# Called when the node enters the scene tree for the first time.
func _take_dmg(dmg):
	emit_signal("dmg",10)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
