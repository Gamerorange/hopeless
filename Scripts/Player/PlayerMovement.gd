extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var speed = 200
var movement = Vector2 (0,0)

signal health_update
var max_health = 500
var current_health = 300
var health_per_lvl = 50

var attack_speed = 1
var animation_time = 1.2

var current_look_direction ="S"
var print_buffer = 300
var current_frame = 0

signal xp_update
var current_xp = 0
var xp_next_lvl = 100
var xp_multiplier = 1.2

signal mana_update
var current_mana = 50
var max_mana = 100
var mana_per_lvl = 50

#characterstats
var player_name = "Gustav"
var player_life_reg = 0
var player_mana_reg = 0
var current_level = 1

onready var attack_field_east = $AttackAreaEast/AttackEast
onready var attack_field_north = $AttackAreaNorth/AttackNorth
onready var attack_field_south = $AttackAreaSouth/AttackSouth
onready var attack_field_west = $AttackAreaWest/AttackWest
onready var animation_player = $AnimatedSprite
onready var lvl_up_particles = $LVL_UP


var is_attacking = false
var is_dead = false



# Called when the node enters the scene tree for the first time.
func _ready():
	yield (get_tree().current_scene,"ready")
	emit_signal("health_update", current_health, max_health)
	emit_signal("mana_update", current_mana, max_mana)
	emit_signal("XP_update", current_xp, xp_next_lvl)


func give_item(item,amount):
	if item == "XP":
		_get_XP(amount)
		

func current_look_direction_lookup():
	if Input.is_action_pressed("ui_left"):
		current_look_direction = "W"
	if Input.is_action_pressed("ui_right"):
		current_look_direction = "E"	
	if Input.is_action_pressed("ui_up"):
		current_look_direction = "N"
	if Input.is_action_pressed("ui_down"):
		current_look_direction = "S"
	
func enable_relevant_attack_field():
	if current_look_direction == "S":
		attack_field_south.disabled = false
	elif current_look_direction == "N":
		attack_field_north.disabled = false
	elif current_look_direction == "W":
		attack_field_west.disabled = false
	elif current_look_direction == "E":
		attack_field_east.disabled = false	

func moving():
	movement = Vector2 (0,0)
	if is_attacking:
		return movement
		
	current_look_direction_lookup()

	if Input.is_action_just_pressed("ui_attack"):
		is_attacking = true
		enable_relevant_attack_field()
		animation_player.speed_scale = 5

	if Input.is_action_pressed("ui_up"):
		movement.y = -speed
	elif Input.is_action_pressed("ui_down"):
		movement.y = speed
	if Input.is_action_pressed("ui_left"):
		movement.x = -speed
	elif Input.is_action_pressed("ui_right"):
		movement.x = speed
	
	#movement = movement.normalized()
	#movement = movement * speed
	return movement

func animation():
	if is_dead == false:
		var type = "Walking_"
		if movement.x == 0 and movement.y == 0 and not is_attacking:
			type = "Walking_"
		elif not is_attacking :
			pass
			#type = "Idling"
		else:
			type = "Attack_"
		animation_player.play(type + current_look_direction)
		
func take_dmg(amount):
	current_health -= amount
	
	emit_signal("health_update",current_health, max_health)
	if current_health <= 0:
		die()	
		current_health = 0
	

func die():
	if is_dead == false:
		animation_player.play("Death")
		is_dead = true
		#get_tree().paused = true
		var GUI = get_parent().get_node("CanvasLayer/GUI")
		GUI.dead_player()
	
func _get_XP(amount):
	current_xp += amount
	if current_xp >= xp_next_lvl:
		_lvl_up()
	emit_signal("XP_update", current_xp, xp_next_lvl)


func _lvl_up():
	lvl_up_particles.emitting = true
	
	current_xp -= xp_next_lvl
	xp_next_lvl = round(xp_next_lvl * xp_multiplier)
	
	max_health += health_per_lvl
	current_health = max_health
	
	max_mana += mana_per_lvl
	current_mana = max_mana
	
	#GUI.updateProgressBar(currentHealth, max_health, "Health")
	emit_signal("health_update", current_health, max_health)
	emit_signal("mana_update", current_mana, max_mana)
	

	
func _physics_process(delta):
	movement = moving()
	animation()
	movement = move_and_slide(movement)
	#if collision:
	#	if "Demon" in collision.collider.name :
		#	_take_dmg(1)
			#if collision.collider.has_method("take_dmg"):
			#	collision.collider.take_dmg(10)
	
	var current_frame_number = Engine.get_frames_per_second()
	current_frame += 1
	if current_frame > print_buffer:
		current_frame =0
		print ("fps:",current_frame_number)



func _on_AnimatedSprite_animation_finished():
	if "Attack" in animation_player.animation:
		is_attacking = false
		animation_player.speed_scale = 1
		attack_field_east.disabled = true;
		attack_field_north.disabled = true;
		attack_field_south.disabled = true;
		attack_field_west.disabled = true;
	if animation_player.animation == "Death":
		queue_free()


func _on_AttackAreaEast_area_entered(area):
	if area.is_in_group("hurtBox"):
		area._take_dmg(10)



func _on_Demon_sendXPValue(XP):
	_get_XP(XP)


func _on_AttackAreaNorth_area_entered(area):
	if area.is_in_group("hurtBox"):
		area._take_dmg(10)

func _on_AttackAreaSouth_area_entered(area):
	if area.is_in_group("hurtBox"):
		area._take_dmg(10)


func _on_AttackAreaWest_area_entered(area):
	if area.is_in_group("hurtBox"):
		area._take_dmg(10)
